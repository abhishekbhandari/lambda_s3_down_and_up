import json
import boto3
import os
import base64
from botocore.exceptions import ClientError

#########
# Use this code snippet in your app.
# If you need more information about configurations or implementing the sample code, visit the AWS docs:   
# https://aws.amazon.com/developers/getting-started/python/




def get_secret():

    secret_name = "test/pythonScript/iamuser"
    region_name = "ap-south-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    # We rethrow the exception by default.

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS key.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            print(secret)
            return secret
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            return decoded_binary_secret
            
    # Your code goes here. 

def download_and_upload_s3_folder(s3,download_from_bucket_name,upload_to_bucket_name, s3_folder, local_dir=None):
    """
    Download the contents of a folder directory into memory
    and upload to another S3 Bucket 
    Args:
        download_from_bucket_name: the name of the s3 bucket to download from
        s3_folder: the folder path in the s3 bucket to be downloaded
        local_dir: a relative or absolute directory path in the local file system
        upload_to_bucket_name: the name of bucket to upload the files into
    """
    download_from_bucket = s3.Bucket(download_from_bucket_name)
    upload_to_bucket = s3.Bucket(upload_to_bucket_name)
    for obj in download_from_bucket.objects.filter(Prefix=s3_folder):
        target = obj.key if local_dir is None \
            else os.path.join(local_dir, os.path.relpath(obj.key, s3_folder))
        if not os.path.exists(os.path.dirname(target)):
            os.makedirs(os.path.dirname(target))
        print("Obj Key ",obj.key)
        print("target ---->",target)
        if obj.key[-1] == '/':
            continue
        download_from_bucket.download_file(obj.key, target)
        upload_file_to_s3(upload_to_bucket=upload_to_bucket,objKey=obj.key,target=target)

def upload_file_to_s3(upload_to_bucket,objKey,target):
    """
    upload file to Specified s3 bucket
    upload_to_bucket:Object of s3 bucket in which upload take place,
    objKey:Name of file 
    target:Combined Path of dir and file name
    """
    result=upload_to_bucket.upload_file(target,objKey)
    print(result)

if __name__=="__main__":
    try:
        secret=json.loads(get_secret())
        s3 = boto3.resource('s3',aws_access_key_id=secret["Access key ID"],aws_secret_access_key=secret["Secret access key"]) 
        download_and_upload_s3_folder(s3=s3,download_from_bucket_name='mydemofolder',upload_to_bucket_name='test-upload-from-lambda', s3_folder='dir-1/', local_dir='/Users/abhishek/Downloads/AWS/')
    except Exception as err:
        print("Operation Failed :",str(err))    